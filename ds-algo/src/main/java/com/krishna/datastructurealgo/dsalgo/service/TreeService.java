package com.krishna.datastructurealgo.dsalgo.service;

import com.krishna.datastructurealgo.dsalgo.model.Tree;

public interface TreeService {
	
	public Tree addNode(int value);

	public Tree deleteNode(int value);

	public Tree getNode(int value);

	public Tree addAvlNode(int value);
	
}
