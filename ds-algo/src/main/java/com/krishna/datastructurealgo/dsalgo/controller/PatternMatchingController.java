package com.krishna.datastructurealgo.dsalgo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.krishna.datastructurealgo.dsalgo.constants.AppConstants;
import com.krishna.datastructurealgo.dsalgo.datastructure.LinkedList;
import com.krishna.datastructurealgo.dsalgo.model.HashNode;
import com.krishna.datastructurealgo.dsalgo.service.HashingService;
import com.krishna.datastructurealgo.dsalgo.service.LinkedListService;
import com.krishna.datastructurealgo.dsalgo.service.PatternService;

@RestController
public class PatternMatchingController {
	
	@Autowired
	private PatternService patternService;
	
	@GetMapping(value  = AppConstants.PATTERN + AppConstants.MATCH)
	public Integer addNode(@RequestParam("pattern") String pattern , @RequestParam("string") String string)
	{
		return patternService.findPositionPatternInStringIfAny(pattern, string);
	}

}
