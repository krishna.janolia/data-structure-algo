package com.krishna.datastructurealgo.dsalgo.service;

public interface PeakService {
	
	public int getAPeak(int ar[]);
}
