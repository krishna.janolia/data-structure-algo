package com.krishna.datastructurealgo.dsalgo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.krishna.datastructurealgo.dsalgo.constants.AppConstants;
import com.krishna.datastructurealgo.dsalgo.datastructure.LinkedList;
import com.krishna.datastructurealgo.dsalgo.service.LinkedListService;

@RestController
public class LinkedListController {
	
	@Autowired
	private LinkedListService linkedListService;
	
	@GetMapping(value  = AppConstants.LINKED_LIST + AppConstants.NAME + AppConstants.SURNAME)
	public LinkedList addNode(@PathVariable("name") String name , @PathVariable("surname") String surname)
	{
		return linkedListService.AddNode(name, surname);
	}

}
