package com.krishna.datastructurealgo.dsalgo.service;

public interface PatternService {
	
	Integer  findPositionPatternInStringIfAny(String pattern , String string);
	
}
