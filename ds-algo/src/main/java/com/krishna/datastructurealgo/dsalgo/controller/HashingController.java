package com.krishna.datastructurealgo.dsalgo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.krishna.datastructurealgo.dsalgo.constants.AppConstants;
import com.krishna.datastructurealgo.dsalgo.datastructure.LinkedList;
import com.krishna.datastructurealgo.dsalgo.model.HashNode;
import com.krishna.datastructurealgo.dsalgo.service.HashingService;
import com.krishna.datastructurealgo.dsalgo.service.LinkedListService;

@RestController
public class HashingController {
	
	@Autowired
	private HashingService hashingService;
	
	@GetMapping(value  = AppConstants.HASHING + AppConstants.ADD+AppConstants.ELEMENT)
	public HashNode[] addNode(@RequestParam("key") int key , @RequestParam("value") int value)
	{
		return hashingService.addNode(key, value);
	}

}
