package com.krishna.datastructurealgo.dsalgo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.krishna.datastructurealgo.dsalgo.constants.AppConstants;
import com.krishna.datastructurealgo.dsalgo.service.PeakService;

@RestController
public class PeakFindingController {
	
	@Autowired
	private PeakService peakService;
	
	@PostMapping(value  = AppConstants.PEAK_FIND+ AppConstants.ONED)
	public int getPeak(@RequestBody int[] ar)
	{
		return peakService.getAPeak(ar);
	}

}
