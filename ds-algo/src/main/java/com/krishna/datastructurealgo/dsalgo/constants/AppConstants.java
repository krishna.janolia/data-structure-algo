package com.krishna.datastructurealgo.dsalgo.constants;

public class AppConstants {
	
	public final static String LINKED_LIST ="/linkedlist";
	public static final String NODE = "/{node}";
	public static final String NAME = "/name/{name}";
	public static final String SURNAME = "/surname/{surname}";
	public static final String PEAK_FIND = "/peak/find";
	public static final String ONED = "/oneds";
	public static final String INSEERTION_SORT = "/insertion/sort";
	public static final String MERGE_SORT = "/merge/sort";
	public static final String HEAP_SORT = "/heap/sort";
	public static final String ADD = "/add";
	public static final String TREE = "/tree";
	public static final String GET = "/get";
	public static final String DELETE = "/delete";
	public static final String AVL = "/avl";
	public static final String COUNTING_SORT = "/couting/sort";
	public static final String INDEX_SORT = "/index/sort";
	public static final String HASHING = "/hashing";
	public static final String ELEMENT = "/element";
	public static final String MATCH = "/match";
	public static final String PATTERN = "/pattern";
}
