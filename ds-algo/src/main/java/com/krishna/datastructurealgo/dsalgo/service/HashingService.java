package com.krishna.datastructurealgo.dsalgo.service;

import com.krishna.datastructurealgo.dsalgo.model.HashNode;

public interface HashingService {
	
	public HashNode [] addNode(int key, int value);	
}
